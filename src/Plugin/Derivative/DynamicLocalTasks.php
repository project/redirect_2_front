<?php

namespace Drupal\redirect_2_front\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines dynamic local tasks.
 */
class DynamicLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * A config factory instance.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    $config = $this->configFactory->get('redirect_2_front.settings');
    foreach ($config->get('entity_type') as $type) {

      $id = 'redirect_2_front.' . $type;
      $this->derivatives[$id] = $base_plugin_definition;
      $this->derivatives[$id]['title'] = t('View in front');
      $this->derivatives[$id]['route_name'] = "entity." . $type . ".redirect_2_front";
      $this->derivatives[$id]['base_route'] = "entity." . $type . ".canonical";
      $this->derivatives[$id]['route_parameters'] = [
        'type' => $type,
      ];
    }
    return $this->derivatives;
  }

}
