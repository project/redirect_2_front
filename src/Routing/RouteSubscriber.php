<?php

namespace Drupal\redirect_2_front\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for Redirect_2_front routes.
 *
 * @see \Drupal\redirect_2_front\Controller\Front
 * @see \Drupal\redirect_2_front\Plugin\Derivative\DynamicLocalTasks
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * A config factory instance.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // Alter route depending settings.
    $config = $this->configFactory->get('redirect_2_front.settings');

    foreach ($config->get('entity_type') as $type) {
      $route = new Route('/r2f/' . $type . '/{' . $type . '}');
      $route
        ->addDefaults([
          '_controller' => '\Drupal\redirect_2_front\Controller\Front::goToFront',
          '_title' => t('View in front'),
        ])
        ->addRequirements([
          '_permission' => 'redirect_2_front go',
        ])
        ->setOption('_admin_route', TRUE)
        ->setOption('r2f_type', $type)
        ->setOption('parameters', [
          $type => ['type' => 'entity:' . $type],
        ]);

      $collection->add("entity.$type.redirect_2_front", $route);

    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', 100];
    return $events;
  }

}
