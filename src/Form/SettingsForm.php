<?php

namespace Drupal\redirect_2_front\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * A bundle manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * A entity type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $bundle_info) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'redirect_2_front.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('redirect_2_front.settings');
    $form = parent::buildForm($form, $form_state);

    // Build entity type.
    $entity_type_definitions = $this->entityTypeManager->getDefinitions();;
    $type_options = [];
    foreach ($entity_type_definitions as $type => $type_info) {
      $type_options[$type] = $type_info->getLabel()->render();
    }
    if (!empty($type_options)) {
      asort($type_options);
    }
    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#options' => $type_options,
      '#default_value' => $config->get('entity_type'),
      '#required' => TRUE,
      '#multiple' => TRUE,
    ];
    $form['url_front'] = [
      '#type' => 'url',
      '#default_value' => $config->get('url_front'),
      '#required' => TRUE,
      '#title' => $this->t('Front url'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('redirect_2_front.settings')
      ->set('url_front', $form_state->getValue('url_front'))
      ->set('entity_type', $form_state->getValue('entity_type'))
      ->save();
    // @todo clear cache dynamic task.
  }

}
