<?php

namespace Drupal\redirect_2_front;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manipulates entity type information.
 *
 * This class contains primarily bridged hooks for compile-time or
 * cache-clear-time hooks. Runtime hooks should be placed in EntityOperations.
 */
class EntityTypeInfo implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The config factory instance.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(AccountInterface $current_user, ConfigFactoryInterface $config_factory) {
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('config.factory')
    );
  }

  /**
   * Adds redirect_2_front links to appropriate entity types.
   *
   * This is an alter hook bridge.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
   *   The master entity type list to alter.
   *
   * @see hook_entity_type_alter()
   */
  public function entityTypeAlter(array &$entity_types) {
    $config = $this->configFactory->get('redirect_2_front.settings');
    foreach ($config->get('entity_type') as $type) {
      if (empty($entity_types[$type])) {
        continue;
      }
      $entity_type = $entity_types[$type];
      if (($entity_type->getFormClass('default') || $entity_type->getFormClass('edit')) && $entity_type->hasLinkTemplate('edit-form')) {
        $entity_type->setLinkTemplate('redirect-2-front', "/r2f/$type/{$type}");
      }
    }
  }

  /**
   * Adds redirect to front operations on entity that supports it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An array of operation definitions.
   *
   * @return array
   *   An array of operation definitions.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   *
   * @see hook_entity_operation()
   */
  public function entityOperation(EntityInterface $entity) {
    $operations = [];
    if ($this->currentUser->hasPermission('redirect_2_front go') && $entity->hasLinkTemplate('redirect-2-front')) {
      $operations['redirect_2_front'] = [
        'title' => $this->t('View in front'),
        'weight' => 100,
        'url' => $entity->toUrl('redirect-2-front'),
      ];
    }

    return $operations;
  }

}
