<?php

namespace Drupal\redirect_2_front\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * Controller to generate list of channels URLs.
 */
class Front extends ControllerBase {

  /**
   * A config factory instance.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A path alias manager instance.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $pathAliasManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, AliasManagerInterface $path_alias_manager) {

    $this->configFactory = $config_factory;
    $this->pathAliasManager = $path_alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('path.alias_manager')
    );
  }

  /**
   * Redirect to front.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route parameters.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|\Zend\Diactoros\Response\RedirectResponse
   *   Redirection to front or message error.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function goToFront(RouteMatchInterface $route_match) {

    // Get settings.
    $config = $this->configFactory->get('redirect_2_front.settings');
    // Get the current entity by route match.
    $parameter_name = $route_match->getRouteObject()->getOption('r2f_type');
    $entity = $route_match->getParameter($parameter_name);
    if ($entity instanceof EntityInterface) {

      // Build the url.
      $path = $entity->toUrl()->toString();
      $alias = $this->pathAliasManager->getAliasByPath($path);
      $url = $config->get('url_front');
      if (!empty($alias)) {
        $url .= $alias;
      }
      else {
        $url .= $path;
      }
      return new RedirectResponse($url);
    }
    return t("You doesn't request an entity redirection");
  }

}
