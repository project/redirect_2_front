CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Developers
 * Maintainers

INTRODUCTION
------------
This module provide a entity tab to view in front in case of drupal headless.


REQUIREMENTS
------------

Path and pathauto.

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. For further
information, see:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules

CONFIGURATION
-------------

-Go to admin/config/redirect-2-front/settings
-Choose the entity type for which you want new tab
-Fill the front url
-For now you must cache clear manually after changing this settings.

DEVELOPERS
----------


MAINTAINERS
-----------
Current maintainers:
  * Thomas MUSA - https://www.drupal.org/u/musathomas
